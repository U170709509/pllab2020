#include <stdio.h>

int nrDigits (int num)

{
    static int count=0;

    if (num>0 )
    {
        count++;
        nrDigits(num/10);
    }
    else
    {

        return count;
    }
}

int main()
{
    int number;
    int count=0;
    printf("Enter a positive integer number:");

    scanf("%d ", &number);
    count = nrDigits(number);
    printf("  number digit  %d is : ", count);


    return 0 ;

}
/* Write a recursive function int nrDigits(int num) that counts the total number of digits of a number. Your
program should read a positive integer value from the user, calls nrDigits function with this value as an
argument and prints the result in the given format.
Sample Output:
Enter a positive integer number: 456654
Total digits in number 456654 is: 6 */


